use std::collections::VecDeque;
use std::fs::File;
use std::io::{stdout, Write};
use std::path::{Path, PathBuf};
use std::process::exit;
use std::sync::mpsc::{Receiver, Sender};
use std::sync::{mpsc, Arc, RwLock};
use std::thread::{self, sleep};
use std::time::Duration;

use clap::{Parser, Subcommand};
use cpal::traits::{DeviceTrait, StreamTrait};
use cpal::{self, traits::HostTrait};
use cpal::{OutputCallbackInfo, StreamConfig};
use crossterm::cursor::{Hide, MoveTo, MoveToColumn, MoveToNextLine, MoveToRow, Show};
use crossterm::event::{Event, KeyCode, KeyModifiers, MediaKeyCode};
use crossterm::style::{Print, PrintStyledContent, Stylize};
use crossterm::terminal::{
    disable_raw_mode, enable_raw_mode, size, Clear, ClearType, EnterAlternateScreen,
};
use crossterm::{execute, queue};
use minimp3::Decoder;
use rand::{seq::SliceRandom, thread_rng};
use serde::{Deserialize, Serialize};
use simple_mutex::Mutex as FairMutex;

#[derive(Subcommand)]
enum CliSubcommand {
    #[command(name = "a", about = "Annotate audio files")]
    Annotate {
        path: Vec<PathBuf>,
        #[arg(
            short = 'b',
            long = "bufsize",
            default_value_t = 1000000000,
            help = "Initial size of audio buffer"
        )]
        initial_buffer_size: usize,
    },
    #[command(name = "d", about = "Delete associated annotations")]
    Delete { path: Vec<PathBuf> },

    #[command(name = "g", about = "Get annotations and print as JSON")]
    Get { path: Vec<PathBuf> },
}

#[derive(Parser)]
#[command(author, version, about, long_about = None)]
struct Cli {
    #[command(subcommand)]
    command: CliSubcommand,
}

#[derive(Debug, Deserialize, PartialEq, Serialize)]
enum Class {
    /// A third-party advertisement, including those read by the host
    Advertisement,
    // Not incidental music: the main content
    Music,

    // Including incidental music, self-promos, and main content
    Speech,
}
impl Class {
    fn sym(&self) -> char {
        match self {
            Self::Advertisement => '🥫',
            Self::Music => '🎶',
            Self::Speech => '💬',
        }
    }
}

/// Input thread commands sent to main thread
#[derive(Debug, PartialEq)]
enum Command {
    DisplayHelp,
    FastForward,
    Rewind,
    AnnotateSince(Class),
    AnnotateFile(Class),
    // AnnotateYesNow,
    // AnnotateNoNow,
    NextFile,
    Play,
    Pause,
    PlayPause,
    Quit,
    ResizeTerminal(u16, u16),
    TogglePlaybackSpeed,
    Undo,
}

impl TryFrom<char> for Command {
    type Error = ();
    fn try_from(c: char) -> Result<Self, Self::Error> {
        match c {
            ',' => Ok(Self::Rewind),
            '.' => Ok(Self::FastForward),
            '>' => Ok(Self::NextFile),
            'a' => Ok(Self::AnnotateSince(Class::Advertisement)),
            'm' => Ok(Self::AnnotateSince(Class::Music)),
            's' => Ok(Self::AnnotateSince(Class::Speech)),
            'M' => Ok(Self::AnnotateFile(Class::Music)),
            'S' => Ok(Self::AnnotateFile(Class::Speech)),
            'q' => Ok(Self::Quit),
            'u' => Ok(Self::Undo),
            ' ' => Ok(Self::PlayPause),
            '?' => Ok(Self::DisplayHelp),
            ';' => Ok(Self::TogglePlaybackSpeed),
            _ => Err(()),
        }
    }
}

impl TryFrom<MediaKeyCode> for Command {
    type Error = ();
    fn try_from(m: MediaKeyCode) -> Result<Self, Self::Error> {
        match m {
            MediaKeyCode::Play => Ok(Command::Play),
            MediaKeyCode::Pause => Ok(Command::Pause),
            MediaKeyCode::PlayPause => Ok(Command::PlayPause),
            _ => Err(()),
        }
    }
}

impl TryFrom<Event> for Command {
    type Error = ();
    fn try_from(e: Event) -> Result<Self, Self::Error> {
        match e {
            Event::Key(evt)
                if evt.code == KeyCode::Char('c')
                    && evt.modifiers.contains(KeyModifiers::CONTROL) =>
            {
                Ok(Command::Quit)
            }
            Event::Key(evt) => match evt.code {
                KeyCode::Char(c) => Command::try_from(c),
                KeyCode::Media(media) => Command::try_from(media),
                KeyCode::Pause => Ok(Command::PlayPause),
                _ => Err(()),
            },
            Event::Resize(columns, rows) => Ok(Command::ResizeTerminal(columns, rows)),
            _ => Err(()),
        }
    }
}

#[derive(Deserialize, Serialize)]
struct Annotation {
    start: usize,
    end: usize,
    class: Class,
}
impl Annotation {
    fn contains(&self, idx: usize) -> bool {
        self.start <= idx && idx <= self.end
    }
}

impl std::fmt::Display for Annotation {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> Result<(), std::fmt::Error> {
        write!(
            f,
            "{}{}:{} {:?}",
            self.class.sym(),
            self.start,
            self.end,
            self.class
        )
    }
}

const MOVEMENT_DURATION_SECONDS: usize = 15;
const XATTR_NAME: &'static str = "user.ad-annotation";

/// Cleanly exit the program
fn quit() {
    disable_raw_mode().unwrap();
    execute!(std::io::stdout(), MoveToNextLine(2), MoveToColumn(0), Show).unwrap();
    exit(0);
}

fn main() {
    let cli = Cli::parse();

    match cli.command {
        CliSubcommand::Annotate {
            path,
            initial_buffer_size,
        } => annotate(path, initial_buffer_size),
        CliSubcommand::Delete { path } => {
            unannotate(path);
        }
        CliSubcommand::Get { path } => {
            get(path);
        }
    }
}

fn annotate(mut path: Vec<PathBuf>, initial_buffer_size: usize) {
    enable_raw_mode().unwrap();
    let mut stdout = stdout();
    execute!(stdout, Hide, EnterAlternateScreen).unwrap();

    let (terminal_columns, terminal_rows) = size().unwrap();
    let terminal_rows = Arc::new(FairMutex::new(terminal_rows));
    let terminal_columns = Arc::new(FairMutex::new(terminal_columns));

    let (input_tx, input_rx): (Sender<Command>, Receiver<Command>) = mpsc::channel();

    let _input_thread = thread::spawn(move || loop {
        let evt = crossterm::event::read().unwrap();
        if let Ok(cmd) = Command::try_from(evt) {
            input_tx.send(cmd).unwrap();
        }
    });

    let mut rng = thread_rng();

    path.shuffle(&mut rng);

    let mut path_iter = path.into_iter();
    let path: Arc<FairMutex<Option<PathBuf>>> = Arc::new(FairMutex::new(None));

    let idx: Arc<FairMutex<usize>> = Arc::new(FairMutex::new(0));

    let pending_movement: Arc<FairMutex<isize>> = Arc::new(FairMutex::new(0));

    let mut paused = false;

    let buf: Arc<FairMutex<Vec<i16>>> =
        Arc::new(FairMutex::new(Vec::with_capacity(initial_buffer_size)));

    let host = cpal::default_host();

    let device = host
        .default_output_device()
        .expect("failed to find output device");

    let config: StreamConfig = device.default_output_config().unwrap().into();

    let output_channels: usize = 2;

    // The number of channels in the file being read; initially unknown
    let mp3_channels: Arc<FairMutex<Option<usize>>> = Arc::new(FairMutex::new(None));

    let sample_rate: Arc<FairMutex<usize>> = Arc::new(FairMutex::new(44100));

    // Multiplier that determines rate of playback
    let playback_speed = Arc::new(RwLock::new(1usize));

    // The audio output stream / thread
    let stream = {
        let idx = Arc::clone(&idx);
        let buf = Arc::clone(&buf);
        let mp3_channels = Arc::clone(&mp3_channels);
        let pending_movement = Arc::clone(&pending_movement);
        let sample_rate = Arc::clone(&sample_rate);
        let terminal_columns = Arc::clone(&terminal_columns);
        let terminal_rows = Arc::clone(&terminal_rows);
        let playback_speed = Arc::clone(&playback_speed);
        device
            .build_output_stream(
                &config,
                move |data: &mut [i16], _: &OutputCallbackInfo| {
                    // Sleep until the buffer has been sufficiently populated and the input channels are known
                    {
                        let idx = *idx.lock();

                        while {
                            let buf = buf.lock();
                            buf.is_empty() || buf.len() < idx
                        } || {
                            let mp3_channels = *mp3_channels.lock();
                            mp3_channels.is_none()
                        } {
                            sleep(Duration::from_millis(50));
                        }
                    }

                    let mp3_channels = mp3_channels.lock().unwrap();

                    // Step through sample by sample, taking channel interleaving into account
                    for out_frame in data.chunks_mut(output_channels) {
                        // Get the current playback index, adjusted for any pending movements
                        let mut idx = {
                            let mut movement = pending_movement.lock();
                            let mut idx = idx.lock();
                            let sample_rate = *sample_rate.lock() as isize;

                            let samples_moved: isize = *movement
                                * mp3_channels as isize
                                * MOVEMENT_DURATION_SECONDS as isize
                                * sample_rate;

                            *idx = (*idx).saturating_add_signed(samples_moved);
                            *movement = 0;
                            idx
                        };

                        let buf = buf.lock();

                        match mp3_channels {
                            1 => {
                                let sample = buf[*idx];
                                out_frame[0] = sample;
                                out_frame[1] = sample;
                            }
                            2 => {
                                for channel in 0..2 {
                                    out_frame[channel] = buf[*idx + channel];
                                }
                            }
                            _ => panic!(
                                "Tried to read mp3 file with {} channels; max is 2",
                                mp3_channels
                            ),
                        }

                        *idx += *playback_speed.read().unwrap() * mp3_channels;

                        {
                            let mut stdout = std::io::stdout().lock();

                            let status = format!(
                                "{} / {} {:.1}%",
                                *idx,
                                buf.len(),
                                100f64 * *idx as f64 / buf.len() as f64
                            );
                            let padding =
                                " ".repeat(*terminal_columns.lock() as usize - status.len());

                            execute!(
                                stdout,
                                MoveTo(0, *terminal_rows.lock()),
                                Print(status),
                                Print(padding)
                            )
                            .unwrap();
                        }
                    }
                },
                |err| {
                    execute!(std::io::stdout(), Print("Audio stream error"), Print(err)).unwrap();
                },
                None,
            )
            .unwrap()
    };

    // Thread that reads and decodes audio files
    let _dataload_thread = {
        let buf = Arc::clone(&buf);
        let path = Arc::clone(&path);
        thread::spawn(move || {
            let mut stdout = std::io::stdout();
            let cookie = magic::Cookie::open(magic::CookieFlags::MIME_TYPE).unwrap();
            cookie.load(&vec!["/usr/share/misc/magic.mgc"]).unwrap();

            loop {
                while buf.lock().is_empty() {
                    if let Some(pathbuf) =
                        path_iter.find(|p| cookie.file(p).unwrap() == "audio/mpeg")
                    {
                        let mut path = path.lock();
                        *path = Some(pathbuf);

                        execute!(
                            stdout,
                            MoveTo(0, 2),
                            Print("🔊"),
                            Print(path.as_ref().unwrap().display())
                        )
                        .unwrap();

                        let file = File::open(path.as_ref().unwrap()).unwrap();

                        // If there are no annotations for the file, load it and begin the process of annotating;
                        if xattr::get(path.as_ref().unwrap(), "user.ad-annotation")
                            .unwrap()
                            .is_none()
                        {
                            let mut mp3dec = Decoder::new(file);

                            {
                                buf.lock().clear();

                                let mut is_first = true;

                                while let Ok(frame) = mp3dec.next_frame() {
                                    buf.lock().extend_from_slice(&frame.data);

                                    // Get settings only from the first frame
                                    if is_first {
                                        is_first = false;

                                        let mut mp3_channels = mp3_channels.lock();
                                        *mp3_channels = Some(frame.channels);

                                        let mut sample_rate = sample_rate.lock();
                                        *sample_rate = frame.sample_rate as usize;
                                    }
                                }
                            }
                        } else {
                            execute!(stdout, MoveTo(0, 2), Print("\t🗃 Annotations Found")).unwrap();
                        }
                    } else {
                        quit();
                    }
                } // while buf empty

                thread::sleep(Duration::from_millis(50));
            }
        })
    };

    // The annotations made thus far, kept in order of start index
    // The timespans covered must be disjoint
    let mut annotations: Vec<Annotation> = Vec::new();

    /// Check that the annotations fully cover the file
    fn annotation_complete(annotations: &Vec<Annotation>, buf_len: usize) -> bool {
        if annotations.is_empty() {
            return false;
        }

        if annotations[0].start != 0 {
            return false;
        }

        for (i, a) in annotations.iter().enumerate().skip(1) {
            if a.start != annotations[i - 1].end {
                return false;
            }
        }

        if annotations[annotations.len() - 1].end != buf_len {
            return false;
        }

        true
    }

    // Write the header
    execute!(
        stdout,
        Clear(ClearType::All),
        MoveTo(0, 0),
        PrintStyledContent("🥔audiotater".underline_magenta()),
        MoveToNextLine(1),
    )
    .unwrap();

    let mut log_lines: VecDeque<String> = VecDeque::with_capacity(*terminal_rows.lock() as usize);

    // Add a line to the log area
    let mut log = {
        let terminal_columns = Arc::clone(&terminal_columns);
        let terminal_rows = Arc::clone(&terminal_rows);
        move |s: &str| {
            log_lines.push_back(s.to_string());

            while log_lines.len() > *terminal_rows.lock() as usize - 10 {
                log_lines.pop_front().unwrap();
            }

            let mut stdout = stdout.lock();

            queue!(stdout, MoveToRow(5)).unwrap();

            for line in log_lines.iter() {
                let right_pad_len = *terminal_columns.lock() as usize - line.len();

                let padding = " ".repeat(right_pad_len);

                queue!(
                    stdout,
                    MoveToColumn(0),
                    Print(line),
                    Print(padding),
                    MoveToNextLine(1)
                )
                .unwrap();
            }

            stdout.flush().unwrap();
        }
    };

    /// Write annotations to file extended attributes
    fn write(annotations: &Vec<Annotation>, path: &Path) {
        let annotation_bytes = serde_json::to_vec(annotations).unwrap();

        xattr::set(path, XATTR_NAME, annotation_bytes.as_slice()).unwrap();
    }

    loop {
        if annotations.len() > 0 {
            log("Current annotations:");

            for a in &annotations[..] {
                log(&format!("{}", a));
            }
        } else {
            log("No annotations yet");
        }
        // Block main thread and react to commands received
        match input_rx.recv().unwrap() {
            Command::Rewind => {
                *pending_movement.lock() -= 1;
            }
            Command::FastForward => {
                *pending_movement.lock() += 1;
            }
            Command::AnnotateFile(class) => {
                log(&format!("Annotated File {:?}", class));

                let path_guard = path.lock();
                let path = path_guard.as_ref().unwrap();

                let mut buf = buf.lock();

                annotations.clear();
                annotations.push(Annotation {
                    start: 0,
                    end: buf.len(),
                    class,
                });

                write(&annotations, path.as_path());
                buf.clear();
            }
            Command::AnnotateSince(class) => {
                let end = *idx.lock();

                log(&format!("Annotated {:?}", class));

                let containing_annotation = annotations.iter_mut().find(|a| a.contains(end));

                if let Some(containing_annotation) = containing_annotation {
                    // If we're within an existing annotation, just change its class
                    containing_annotation.class = class;
                } else {
                    let last_annotation = annotations.last();

                    let start = last_annotation.map_or(0, |a| a.end);

                    {
                        let path_guard = path.lock();
                        let path = path_guard.as_ref().unwrap();

                        annotations.push(Annotation { start, end, class });

                        if annotation_complete(&annotations, buf.lock().len()) {
                            write(&annotations, path.as_path());
                            buf.lock().clear();
                        }
                    }
                }
            }
            Command::NextFile => {
                buf.lock().clear();
            }
            Command::Play => {
                stream.play().unwrap();
                paused = false;
            }
            Command::Pause => {
                stream.pause().unwrap();
                paused = true;
            }
            Command::PlayPause => {
                if paused {
                    stream.play().unwrap();
                    paused = false;
                } else {
                    stream.pause().unwrap();
                    paused = true;
                }
            }
            Command::Quit => {
                quit();
            }
            Command::ResizeTerminal(columns, rows) => {
                *terminal_columns.lock() = columns;
                *terminal_rows.lock() = rows;
            }
            Command::TogglePlaybackSpeed => {
                let next = match *playback_speed.read().unwrap() {
                    1 => 2,
                    2 => 4,
                    4 => 1,
                    x => panic!("Unsupported playback speed {}", x),
                };
                *playback_speed.write().unwrap() = next;
            }
            Command::DisplayHelp => {
                log(", Rewind");
                log(". FastForward");
                log("> Next File");
                log("; Toggle Playback Speed");
                log("a Annotate Since: Ad");
                log("m Annotate Since: Music");
                log("s Annotate Since: Speech");
                log("M Annotate File: Music");
                log("S Annotate File: Speech");
                log("Space Play/Pause");
                log("w Save Annotations");
                log("o Load Annotations");
                log("? Display Help");
                log("q Quit");
            }
            Command::Undo => {
                if annotations.len() > 0 {
                    let old = annotations.pop().unwrap();
                    *idx.lock() = old.start;
                }
            }
        }
    } // end input processing loop
}

/// Print annotations from the given paths as JSON
fn get(path: Vec<PathBuf>) {
    let num_paths = path.len();

    println!("[");
    for (i, path) in path.iter().enumerate() {
        if let Some(annotation_bytes) = xattr::get(path.as_path(), XATTR_NAME).unwrap_or_else(|e| {
            panic!(
                "Error getting extended attribute {} for {}: {}",
                XATTR_NAME,
                path.as_path().display(),
                e
            )
        }) {
            // Deserialize then reserialize for output
            let annotations: Vec<Annotation> = serde_json::from_slice(annotation_bytes.as_slice())
                .unwrap_or_else(|e| {
                    panic!(
                        "Error deserializing annotation bytes for {}: {}",
                        path.as_path().display(),
                        e
                    )
                });

            let num_annotations = annotations.len();

            for (j, a) in annotations.into_iter().enumerate() {
                print!("\t{}", serde_json::to_string(&a).unwrap());
                if !(i == num_paths - 1 && j == num_annotations - 1) {
                    print!(",");
                }
                println!();
            }
        }
    }
    println!("]");
}

fn unannotate(path: Vec<PathBuf>) {
    let mut stdout = std::io::stdout();
    for path in path.iter() {
        xattr::remove(path.as_path(), XATTR_NAME).ok(); // ok() just to handle the result; we don't care if it fails or not?
        execute!(
            stdout,
            Print(format!("Deleted annotation for {}\n", path.display()))
        )
        .unwrap();
    }
    quit();
}
